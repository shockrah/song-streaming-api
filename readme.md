# Solo Song Streamer

A completely self hostable song streaming server written in C with [Kore](https://kore.io).

# Tech Stack

* C 

* Kore

* PostgreSQL

* Python - for testing


## API

Certain Routes require authentication but all can be locked behind an authentication wall if required


## Authenticated Routes

> `/playlist/create`
> `/playlist/add/<song_name>`
> `/playlist/remove/<song_name>`


> `/songs/add/<song_name>`
> `/songs/remove<song_name>`


## Open Routes - Can be locked(TBD)

> `/playlist/<name>`
> `/song/<name>`
