#include <kore/kore.h>
#include <kore/http.h>
#include <kore/pgsql.h>
#include <stdio.h>
#include <stdlib.h> // mainly for getenv

#include "./shared/result.h"
#include "./shared/db.h"

// results are made of two fields
// enum Result {Ok/Fail}
// char* reason;
const struct DBResult DB_RESULT_OK = {Ok, NULL}; 

static struct DBResult __db_create_table(const char*);
struct DBResult db_create_playlist(const char*);
struct DBResult db_add_new_song(const uint8_t*, const size_t, const char*);


static struct DBResult 
__db_create_table(const char* name) {
	struct kore_pgsql sql;
	struct DBResult r;
	if(!kore_pgsql_setup(&sql, "db-name", KORE_PGSQL_SYNC)) {
		kore_pgsql_logerror(&sql);
	}
	else {
		const char* q = "SELECT name FROM songs";
		int data_result = kore_pgsql_query_params(&sql, q, 0, 0);
		if(data_result == KORE_RESULT_OK) {
			printf("got stuff from db");
		}
		else {
			printf("issue with db");
		}
	}

	kore_pgsql_cleanup(&sql);
	return r;
}

struct DBResult 
db_create_playlist(const char* plist_name) {
	// we should only create a playlist if it doesn't already exist
	if(plist_name == NULL) {
		struct DBResult ret = {Fail, "Missing required parameter: playlist_name"};
		return ret;
	}
	else {
		return __db_create_table(plist_name);
	}
	return DB_RESULT_OK;
}

void 
db_create_http_response(const struct DBResult* result, struct http_request* req) {
	/*
	 * NOTE: this logic may/may not belong here idk
	 * Populates an http response based on the given db result that we're given
	 */
}

struct DBResult
db_add_new_song(const uint8_t* buffer, const size_t size, const char* name) {
	const char* ins = "INSERT INTO TABLE songs() ";
	kore_log(LOG_INFO, "Running add new song routine");
	return DB_RESULT_OK;
}
