#ifndef AUTH_H
#define AUTH_H


#include <kore/kore.h>
#include <kore/http.h>

/* Does two things on success, populates the request accordingly
 * and does our authentication against our admins section
 */
#define AUTH_SUCCESS 0
#define AUTH_FAIL 1
#define AUTH_GET 2
#define AUTH_POST 3

#define authenticate_post(r) auth_request((AUTH_POST), (r))
#define authenticate_qs(r) auth_request((AUTH_GET), (r))

int auth_request(int type, struct http_request *);
#endif /* AUTH_H */
