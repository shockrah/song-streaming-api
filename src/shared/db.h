#ifndef DB_H
#define DB_H
#include <kore/kore.h>
#include <kore/http.h>

#include "./result.h"


struct DBResult {
	enum Result result;
	char* cause; // remains null iif we succeed
};

struct DBResult db_create_playlist(const char*);

void db_create_http_response(const struct DBResult*, struct http_request*);
#endif /* DB_H */
