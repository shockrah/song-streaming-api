#ifndef RESULT_H
#define RESULT_H

enum Result {
	Ok,	 	// everything fine
	Fail,	// total failure
	Issue	// structurally fine but with logical issues
};

#endif /* RESULT_H */
