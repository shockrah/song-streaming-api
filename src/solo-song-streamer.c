#include <kore/kore.h>
#include <kore/http.h>

#include "./shared/auth.h"
#include "./shared/db.h"

int		playlist_create(struct http_request *);
// TODO
//int		playlist_add_song(struct http_request *);
//int		playlist_remove_song(struct http_request *);
//
//int		songs_add(struct http_request *);
//int		songs_remove(struct http_request *);




int	playlist_create(struct http_request * req) {
	// First we populate parameters form the body
	char* plist_name = NULL;
	struct DBResult db_result;

	if(authenticate_post(req) == AUTH_SUCCESS) {
		http_argument_get_string(req, "name", plist_name);
		db_result = db_create_playlist(plist_name);
		db_create_http_response(&db_result, req);
	}
	else {
		http_response(req, 403, NULL,0);
	}

	return (KORE_RESULT_OK);
}
