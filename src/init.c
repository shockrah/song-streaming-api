#include <kore/kore.h>
#include <kore/pgsql.h>

int init(int);


int
init(int state) {
	kore_log(LOG_INFO, "Starting up db things");
	const char* db_link = getenv("DB_LINK");
	const char* db_name = getenv("DB_NAME");
	if(db_link == NULL)  {
		kore_log(LOG_ERR, "No DB_LINK not found in .env, DB_NAME likely also failed");
	}

	if(kore_pgsql_register(db_name, db_link) == KORE_RESULT_OK) {
		return KORE_RESULT_OK;
	}
	else {
		return KORE_RESULT_ERROR;
	}
}
